(function(){
	
	//AngularJS would be a smart choice if this was a bit more complex
	
	var fetchStackOverflow = function() {
		$('#persistBtn').button('loading');
		$.get("/stack_moblee/v1/fetch")
			.done(function(data, textStatus, jqXHR) {
				$('#fetch-success').show();
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				$('#fetch-error').show();
			})
			.always(function(){
				$('#persistBtn').button('reset');
			});
	};

	$('#persistBtn').click(fetchStackOverflow);
	
})();