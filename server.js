var http = require('http');
var storage = require('node-persist');
var dispatcher = require('./dispatcher');

var PORT = process.env.PORT || 8080; 

console.log('hello mobLee!');

storage.initSync();

var server = http.createServer(function(request, response) {
    dispatcher.dispatch(request, response);
});

server.listen(PORT, function() {
    console.log('server listening on http://localhost:%s', PORT);
});