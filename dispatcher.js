var parse = require('url').parse;
var mime = require('mime');
var stackoverflow = require('./stackoverflow');

var writeFile = function(path, response) {
	//TODO cache the files
	//FIXME loading arbitrary files is a HUGE security risk, but a proper routing library such as Express would take care of it
	require('fs').readFile(path, {encoding: 'UTF-8'}, function(err, file) {
		if(err) {
			response.writeHead(500);
			console.log('Could not read file:', err);
		} else {
			response.writeHead(200, {
				"Content-Type": mime.lookup(path)
			});
			//FIXME got some issues with encoding here
			response.write(file, 'binary');	
		}
		response.end();
	});
};

var dispatch = function(request, response){
	var url = parse(request.url, true);
	switch(url.pathname) {
		case '/': 
			writeFile('./static/index.html', response);
			break;
		case '/stack_moblee/v1/fetch':
			stackoverflow.fetch(response);
			break;
		case '/stack_moblee/v1/question':
			stackoverflow.search(url.query, response);
			break;
		default:
			writeFile('.' + url.pathname, response);
			break;
	}
};

module.exports = {
	dispatch: dispatch
};