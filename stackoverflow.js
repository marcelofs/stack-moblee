var storage = require('node-persist');

var fetch = function(response){
	var url = 'https://api.stackexchange.com/2.2/questions';
	var query = {
		page: 1,
		pagesize: 99,
		order: 'desc',
		sort: 'creation',
		site: 'stackoverflow',
		tagged: process.env.TAG || 'php'
	};
	require('request').get({url: url, qs: query, gzip: true, json: true}, function (error, res, body) {
		if(error) {
			response.writeHead(502);
			console.log('Could not download from stackoverflow:', error);
			response.end();
			return;
		}
		
		body.last_update = Math.floor(new Date() / 1000);
		storage.setItem('stackoverflow-data', body, function(err) {
			if(err) {
				response.writeHead(500);
				console.log('Could not save data:', err);
				response.end();
				return;
			}

			response.writeHead(205);
			response.end();
		});
	});
};

var filterScore = function(array, score) {
	return array.filter(function(item) {
		return item.score > score;
	});
};

var removeExtraData = function(array) {
	// stackoverflow returns a lot of data, 
	// so we'll just copy to a new array instead of deleting
	var clean = [];
	array.forEach(function(item) {
		clean.push({
			question_id: item.question_id,
			title: item.title,
			owner_name: item.owner.display_name,
			score: item.score,
			creation_date: item.creation_date,
			link: item.link,
			is_answered: item.is_answered
		});
	});
	return clean;
};

var sort = function(array, prop) {
	return array.sort(function(a, b){
		return a[prop] > b[prop] ? 1 : a[prop] < b[prop] ? -1 : 0;
	});
};

var search = function(query, response) {
	var allData = storage.getItem('stackoverflow-data');

	var items = query.score ? filterScore(allData.items, query.score) : allData.items; 
	items = removeExtraData(items);
	if(query.sort) {
		items = sort(items, query.sort);
	}
	if(query.page && query.rpp) {
		var max = query.page * query.rpp;
		var min = max - query.rpp;
		items = items.slice(min, max);
	}
	
	response.writeHead(200, {'Content-Type': 'application/json'});
	response.end(JSON.stringify({
		last_update: allData.last_update,
		content: items
	}));
};

module.exports = {
	fetch: fetch,
	search: search
};