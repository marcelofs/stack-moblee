var assert = require('assert');
var request = require('request');

var url = 'http://localhost:8080/stack_moblee/v1/question';

//TODO auto start the server for CI

describe('api', function() {

	it('should be listening', function(done) {
		request(url, function (error, response, body) {
			assert(!error);
			assert.equal(response.statusCode, 200);
			done();
 		});
	});
	
	it('should return json', function(done) {
		request(url, function (error, response, body) {
			assert(canParse(body));
			done();
 		});
	});
	
	function canParse(json) {
	    try {
	        JSON.parse(json);
	    } catch (err) {
	        return false;
	    }
	    return true;
	}
});